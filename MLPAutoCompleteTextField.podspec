Pod::Spec.new do |s|
  s.name         = "MLPAutoCompleteTextField"
  s.version      = "1.6"
  s.summary      = "UITextfield subclass with autocomplete menu."
  s.homepage     = "https://bitbucket.org/omarjalil/mlpautocompletetextfield"
  s.license      = "MIT"
  s.author       = { "Eddy Borja" => "eddyborja@gmail.com" }
  s.source       = { :git => "https://bitbucket.org/omarjalil/mlpautocompletetextfield.git" } 
  s.platform     = :ios, '5.0'
  s.source_files = 'MLPAutoCompleteTextField', '*.{h,m}'
  s.public_header_files = '*.h'
  s.requires_arc = true
end